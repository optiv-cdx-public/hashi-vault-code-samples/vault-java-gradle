package hello;

import com.bettercloud.vault.*;
import com.bettercloud.vault.response.*;
import java.util.HashMap;
import java.util.Map;

public class VaultExample {

    static Vault vault;

    public static void main(String[] args) {

      // Get vault configurations from ENVIRONMENT variables
      String vaultAddress = System.getenv("VAULT_ADDR");
      String vaultToken = System.getenv("VAULT_TOKEN");

      System.out.println("Using Vault at: " + vaultAddress);

      try {

        // Create a connect to Vault
        final VaultConfig config = new VaultConfig()
                                  .address(vaultAddress)
                                  .token(vaultToken)
                                  .build();
        vault = new Vault(config);

        System.out.println("Connected to Vault!");

        final String secretName = "test/hello";

        final Map<String, Object> secretValues = new HashMap<String, Object>();
        secretValues.put("item1", "adp is awesome");
        secretValues.put("item2", "vault rocks!");

        // Write a new secret in Vault and then read it back out
        writeSecret(secretName, secretValues);
        readSecret(secretName);

        // Update the existing secret we just created with new values
        final Map<String, Object> newSecretValues = new HashMap<String, Object>();
        newSecretValues.put("item1", "adp is awesome");
        newSecretValues.put("item2", "hashi vault is super cool");
        newSecretValues.put("item3", "protect your secrets!");

        System.out.println("Updating secret");

        writeSecret(secretName, newSecretValues);
        readSecret(secretName);
      }
      catch(VaultException exception) {
        System.out.println("Failed to connect to Vault");
      }
      
    }

    // Write a secret to Vault
    private static void writeSecret(String name, Map<String, Object> secretData) throws VaultException {

      System.out.println("Writing secret name: " + name);
        System.out.println("Writing secret data: ");

        secretData.entrySet().stream().forEach(secret -> 
          System.out.println(secret.getKey() + ": " + secret.getValue())
        );

        final LogicalResponse writeResponse = vault.withRetries(5, 1000).logical().write(name, secretData);
    } 

    // Read a secret from Vault
    private static void readSecret(String name) throws VaultException {

      System.out.println("Reading secret name: " + name);

      vault.logical().read(name).getData().entrySet().stream().forEach(secret -> 
          System.out.println(secret.getKey() + ": " + secret.getValue())
        );
    }
}