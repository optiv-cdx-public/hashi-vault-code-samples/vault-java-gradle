# Java Vault Example

This repository is meant to demonstrate some of the basic functionality of HashiCorp Vault. Specifically, it demonstrates storing and accessing secrets stored in HashiCorp's KV2 (key/value pair) engine. 

This repository demonstrates the following Vault functionality:

- How to connect a Java application to a running instance of Vault
- How to write a key/value secret to Vault
- How to read a key/value secret from Vault

---

## Java & Gradle Installation

If you do not have Java/Gradle installed on your machine already, please follow the instructions below to install them.

1) Connect to your Linux instance 

2) Install Java JDK

    Depending on what platform you're running, installing Java is a different process. 

    - Amazon Linux
   
        ```bash
        sudo amazon-linux-extras install java-openjdk11
        ```

    In order to continue installing Gradle in the next step, you'll need at least **JDK6 or later**.

    To see which version of java is running type: `java --version`

3) Install SDK & Gradle

    Install SDK (Software Development Kit Manager)

    ``` bash
    curl -s "https://get.sdkman.io" | bash
    source "$HOME/.sdkman/bin/sdkman-init.sh"
    ```

    Verify that SDK was installed properly by running:

    ```bash
    sdk version
    ```

    If SDK was installed properly, you should see something similar to the following:

    ```
    > SDKMAN 5.11.5+713
    ```

    Use SDK to install the latest version of Gradle

    ```bash
    sdk install gradle
    ```

    If installation was successful, you will see the following:

    ```bash
    Installing: gradle 7.0.2
    Done installing!

    Setting gradle 7.0.2 as default.
    ```
---

## Add Vault-Java-Driver to an existing project

To install the Vault-Java-Driver into an existing project, add one of the following to your project:

- Gradle

    ```bash
    dependencies {
        implementation 'com.bettercloud:vault-java-driver:5.1.0'
    }
    ```

- Maven
    ```bash
    <dependency>
        <groupId>com.bettercloud</groupId>
        <artifactId>vault-java-driver</artifactId>
        <version>5.1.0</version>
    </dependency>
    ```

To view additional documentation / how-to's for Vault-Java-Drive, check out their github repository located here: https://github.com/BetterCloud/vault-java-driver

---

## Configure your environment to connect to Vault

Set the following environment variables to configure which Vault server this application connects to. These variables tell your app where your instance of Vault is running, which token to use for authentication and which namespace to use.

```bash
export VAULT_ADDR='http://127.0.0.1:8200'
export VAULT_TOKEN='[TOKEN]'
export VAULT_NAMESPACE=''
```

---

## Create a test secrets engine

Run the following to create a test secrets engine we can read from and write to:

```
vault secrets enable --path=test kv-v2
```

---

## Run the Java app

Build and run the sample Java app. You will see output when the operations perfomed are successful.

```bash
./gradlew build && ./gradlew run
```

If the app ran successfully, you should see the following output:

```bash
Writing secret name: test/hello
Writing secret data:
item2: vault rocks!
item1: adp is awesome
Reading secret name: test/hello
item2: vault rocks!
item1: adp is awesome
Updating secret
Writing secret name: test/hello
Writing secret data:
item2: hashi vault is super cool
item1: adp is awesome
item3: protect your secrets!
Reading secret name: test/hello
item2: hashi vault is super cool
item1: adp is awesome
item3: protect your secrets!
```

To validate the KV was written successfully, you can also:

- Login to the Vault UI: http://127.0.0.1:8200/ui
- Verify via the command line `vault kv get test/hello`
